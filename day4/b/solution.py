with open('input.txt', 'r') as f:
    store_dict = {}
    for line in f.readlines():
        game_num = int(line.strip().split(':')[0].split(' ')[-1])
        winning, have = line.strip().split(':')[-1].strip().split('|')
        win_set = set([int(x) for x in winning.strip().split(' ') if len(x) > 0])
        have_set = set([int(x) for x in have.strip().split(' ') if len(x) > 0])

        winning = len(win_set.intersection(have_set))
        store_dict[game_num] = winning

memo = {len(store_dict) : 0}
for game_num in reversed(range(1, len(store_dict))):
    copies_won = store_dict[game_num]

    for i in range(game_num + 1, min(len(store_dict), game_num + copies_won + 1)):
        copies_won += memo[i]

    memo[game_num] = copies_won

print(sum(memo.values()) + len(store_dict))
