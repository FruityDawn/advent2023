with open('input.txt', 'r') as f:
    sum_ = 0
    for line in f.readlines():
        winning, have = line.strip().split(':')[-1].strip().split('|')
        win_set = set([int(x) for x in winning.strip().split(' ') if len(x) > 0])
        have_set = set([int(x) for x in have.strip().split(' ') if len(x) > 0])

        winning = len(win_set.intersection(have_set))
        if winning > 0:
            sum_ += 2 ** (winning - 1)
print(sum_)

