def process_history(history):
    new_history = []
    for i in range(len(history) - 1):
        new_history.append(history[i + 1] - history[i])

    if all([n == 0 for n in new_history]):
        return history[0] - new_history[0]

    else:
        return history[0] - process_history(new_history)


with open('input.txt', 'r') as f:
    lines = f.read().splitlines()
    sum = 0
    for line in lines:
        his = [int(i) for i in line.split(' ')]
        sum += process_history(his)


print(sum)
