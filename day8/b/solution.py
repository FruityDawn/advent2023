mapping = {}

with open('input.txt', 'r') as f:
    lines = f.read().splitlines()

    for line in lines:
        if '=' in line:
            split_ = line.split('=')
            start = split_[0].strip()
            split2 = split_[1].strip()[1:-1].split(',')
            left = split2[0].strip()
            right = split2[1].strip()
            mapping[start] = {'L': left, 'R': right}
        elif len(line) > 0:
            dirs = line

pos = [p for p in mapping if p[-1] == 'A']

step_counts = []

for p in pos:
    steps = 0
    i = 0
    new_pos = p

    while new_pos[-1] != 'Z':
        next_step = dirs[i]
        new_pos = mapping[new_pos][next_step]
        i = (i + 1) % len(dirs)
        steps += 1

    step_counts.append(steps)

import math
print(math.lcm(*step_counts))

