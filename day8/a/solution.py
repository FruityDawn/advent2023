mapping = {}

with open('input.txt', 'r') as f:
    lines = f.read().splitlines()

    for line in lines:
        if '=' in line:
            split_ = line.split('=')
            start = split_[0].strip()
            split2 = split_[1].strip()[1:-1].split(',')
            left = split2[0].strip()
            right = split2[1].strip()
            mapping[start] = {'L': left, 'R': right}
        elif len(line) > 0:
            dirs = line

pos = 'AAA'
steps = 0
i = 0

while pos != 'ZZZ':
    next_step = dirs[i]
    pos = mapping[pos][next_step]
    steps +=1
    i = (i + 1) % len(dirs)

print(steps)
