res = 0

grid = []


dir_map = {
    # NESW
    0: (-1, 0),
    1: (0, 1),
    2: (1, 0),
    3: (0, -1)
}

connections = {
    # NESW
    'S': [1, 1, 1, 1],
    '|': [1, 0, 1, 0],
    '-': [0, 1, 0, 1],
    'L': [1, 1, 0, 0],
    'J': [1, 0, 0, 1],
    '7': [0, 0, 1, 1],
    'F': [0, 1, 1, 0],
    '.': [0, 0, 0, 0]
}

with open('input.txt', 'r') as f:
    lines = f.read().splitlines()
    for line in lines:
        grid.append([c for c in line])

start = None
for r in range(len(grid)):
    for c in range(len(grid[0])):
        if grid[r][c] == 'S':
            start = (r, c)
            break
    if start is not None:
        break

queue = [(start, 0, (-1, -1), [start])]

start_found = False
final_dist = 0

final_hist = []

traversed = set()

while len(queue) > 0:
    pos, dist, from_, history = queue.pop(-1)
    r,c = pos
    shape = grid[r][c]

    if shape == 'S' and dist > 0:
        start_found = True
        final_dist = dist
        final_hist = history
        break

    for dir in range(4):
        mapping = dir_map[dir]
        other_r = r + mapping[0]

        other_c = c + mapping[1]
        if connections[shape][dir] == 1 and other_r >= 0 and other_r < len(grid) and other_c >= 0 and other_c < len(grid[0]):
            other_shape = grid[other_r][other_c]
            other_dir = (dir + 2) % 4
            from_r, from_c = from_
            traversed_1 = f'{r}-{c}-{from_r}-{from_c}'
            traversed_2 = f'{from_r}-{from_c}-{r}-{c}'
            traversed.add(traversed_1)
            traversed.add(traversed_2)

            traversed_2_next = f'{other_r}-{other_c}-{r}-{c}'
            traversed_1_next = f'{r}-{c}-{other_r}-{other_c}'

            if connections[other_shape][other_dir] == 1 and (traversed_1_next not in traversed) and (traversed_2_next not in traversed):
                queue.append(((other_r, other_c), dist + 1, (r, c), history + [(other_r, other_c)]))


pipes = set(final_hist)
vertical = [[0 for _ in range(len(grid[0]))] for _ in range(len(grid))]
horizontal = [[0 for _ in range(len(grid[0]))] for _ in range(len(grid))]
 
for r in range(len(grid)):
    enclosed = False
    corner = False
    entry_shape = None
    for c in range(len(grid[0])):
        if (r, c) in pipes and grid[r][c] in 'S|':
            enclosed = not enclosed
        elif (r,c) in pipes and grid[r][c] in 'SF7LJ':
            if corner:
                corner = False
                if entry_shape in 'F7' and grid[r][c] not in 'F7' or entry_shape in 'LJ' and grid[r][c] not in 'LJ':
                    enclosed = not enclosed
            else:
                entry_shape = grid[r][c]
                corner = True
        elif (r, c) not in pipes and enclosed:
                horizontal[r][c] = 1


for c in range(len(grid[0])):
    enclosed = False
    corner = False
    entry_shape = None
    for r in range(len(grid)):
        if (r, c) in pipes and grid[r][c] in 'S-':
            enclosed = not enclosed
        elif (r,c) in pipes and grid[r][c] in 'SF7LJ':
            if corner:
                corner = False
                if entry_shape in 'FL' and grid[r][c] not in 'FL' or entry_shape in '7J' and grid[r][c] not in '7J':
                    enclosed = not enclosed
            else:
                entry_shape = grid[r][c]
                corner = True
        elif (r, c) not in pipes and enclosed:
                vertical[r][c] = 1

enclosed_tiles = 0

for r in range(len(grid)):
    for c in range(len(grid[0])):
        if vertical[r][c] == 1 and horizontal[r][c] == 1:
            enclosed_tiles += 1

print(enclosed_tiles)
