map = []

symbols = '!@#$%^&*()_+=/\\'

with open('input.txt', 'r') as f:
    for line in f.readlines():
        map.append([*line.strip()])

width = len(map[0])
height = len(map)

def get_nums(row):
    nums = []
    i = 0

    current_num = None
    start = None
    while(i < len(row)):
        if row[i].isdigit():
            if current_num is None: 
                current_num = row[i]
                start = i
            else:
                current_num += row[i]
        
        if not row[i].isdigit() or i + 1 == len(row):
            if current_num is not None:
                nums.append((current_num, start))
                current_num = None
                start = None

        i += 1

    return nums

def adj_symbol(row, col, map):
    pos = [
            (row - 1, col -1), (row - 1, col), (row - 1, col + 1),
            (row, col - 1),                     (row, col + 1),
            (row + 1, col - 1), (row + 1, col), (row + 1, col + 1)
           ]

    for r, c in pos:
        if r >= 0 and r < height and c >= 0 and c < width and not map[r][c].isdigit() and not map[r][c] == '.':
            return True
    return False

sum = 0

for row in range(height):
    nums = get_nums(map[row])

    for num, start in nums:
        found_symbol = False
        for c in range(start, start + len(num)):
            if adj_symbol(row, c, map):
                found_symbol = True
                break

        if found_symbol:
            sum += int(num)
            print(num)

print(sum)


