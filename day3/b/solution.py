map = []

with open('input.txt', 'r') as f:
    for line in f.readlines():
        map.append([*line.strip()])

width = len(map[0])
height = len(map)

def get_nums(row):
    nums = []
    i = 0

    current_num = None
    start = None
    while(i < len(row)):
        if row[i].isdigit():
            if current_num is None: 
                current_num = row[i]
                start = i
            else:
                current_num += row[i]
        
        if not row[i].isdigit() or i + 1 == len(row):
            if current_num is not None:
                nums.append((current_num, start))
                current_num = None
                start = None

        i += 1

    return nums

def adj_gears(row, col, map):
    gears = set()

    pos = [
            (row - 1, col -1), (row - 1, col), (row - 1, col + 1),
            (row, col - 1),                     (row, col + 1),
            (row + 1, col - 1), (row + 1, col), (row + 1, col + 1)
           ]

    for r, c in pos:
        if r >= 0 and r < height and c >= 0 and c < width and map[r][c] == '*':
            gears.add((r,c))
    return gears

gear_dict = {}

for row in range(height):
    nums = get_nums(map[row])

    for num, start in nums:
        all_gears = set()
        for c in range(start, start + len(num)):
            all_gears = all_gears.union(adj_gears(row, c, map))

        for gear in all_gears:
            if gear not in gear_dict:
                gear_dict[gear] = []

            gear_dict[gear].append(int(num))

sum = 0
for gear in gear_dict.values():
    if len(gear) == 2:
        sum += gear[0] * gear[1]
print(sum)
