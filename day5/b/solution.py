from tqdm import tqdm
with open('input.txt', 'r') as f:
    seeds = []
    maps = dict()
    mapping_order = []

    current_mapping = None
    current_tuples = None
    for line in f.read().splitlines():
        if 'seeds:' in line:
            all_seeds = [int(s) for s in line.split(':')[-1].strip().split(' ')]
            for s in range(int(len(all_seeds) / 2)):
                seeds.append((all_seeds[2 * s], all_seeds[2 * s] + all_seeds[2*s + 1]))
        elif 'map:' in line:
            current_mapping = line[:-(len(' map:'))]
            current_tuples = []
            mapping_order.append(current_mapping)
        elif current_mapping is not None:
            if len(line) > 0:
                    current_tuples.append([int(i) for i in line.split(' ')])
            else:
                    maps[current_mapping] = current_tuples
                    current_mapping = None
                    current_tuples = None

    maps[current_mapping] = current_tuples

def overlap(tup1, tup2):
    x1, y1 = tup1
    x2, y2 = tup2

    if x1 < x2 and y1 < x2 or x1 > y2 and y1 > y2:
        intersection = []
        unions = [(x1,y1)]
    else:
        intersection = [(max(x1, x2), min (y1, y2))]

        unions = []
        left_union = (x1, x2)
        right_union = (y2, y1)

        if left_union[0] < left_union[1]:
            unions.append(left_union)
        if right_union[0] < right_union[1]:
            unions.append(right_union)

    return intersection, unions

def transform(tup, dst, src, rnge):
    x, y = tup
    x = dst + x - src
    y = dst + y - src
    return (x, y)


queue = seeds.copy()
new_queue = []
res = []
for mapping in mapping_order:
    for dst, src, rnge in maps[mapping]:
        while(len(queue) > 0):
            tup = queue.pop()

            intersections, unions = overlap(tup, (src, src + rnge))

            for intersection in intersections:
                res.append(transform(intersection, dst, src, rnge))

            for union in unions:
                new_queue.append(union)

        queue = new_queue.copy()
        new_queue = []

    queue += res
    res = []

final_seeds = [x for (x,y) in queue]
final_seeds.sort()
print(final_seeds[0])

