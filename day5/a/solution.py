with open('input.txt', 'r') as f:
    seeds = []
    maps = dict()
    mapping_order = []

    current_mapping = None
    current_tuples = None
    for line in f.read().splitlines():
        if 'seeds:' in line:
            seeds = [int(s) for s in line.split(':')[-1].strip().split(' ')]
        elif 'map:' in line:
            current_mapping = line[:-(len(' map:'))]
            current_tuples = []
            mapping_order.append(current_mapping)
        elif current_mapping is not None:
            if len(line) > 0:
                    current_tuples.append([int(i) for i in line.split(' ')])
            else:
                    maps[current_mapping] = current_tuples
                    current_mapping = None
                    current_tuples = None

    maps[current_mapping] = current_tuples

def m(x, mapping, maps):
    for dst, src, rnge in maps[mapping]:
        if x >= src and x < src + rnge:
            return dst + x - src
    return x


lowest_loc = None
lowest_seed = None
for seed in seeds:
    final_num = seed

    for mapping in mapping_order:
        final_num = m(final_num, mapping, maps)

    if lowest_loc is None or final_num < lowest_loc:
        lowest_loc = final_num
        lowest_seed = seed

print(lowest_seed)
print(lowest_loc)
