hands = []
bids = []
with open('input.txt', 'r') as f:
    for line in f.read().splitlines():
        hand, bid = line.split(' ')
        hands.append(hand)
        bids.append(int(bid))

tie_mapping = {
    '2': '02',
    '3': '03',
    '4': '04',
    '5': '05',
    '6': '06',
    '7': '07',
    '8': '08',
    '9': '09',
    'T': '10',
    'J': '01',
    'Q': '12',
    'K': '13',
    'A': '14'
}

chars = '23456789TJQKA'

def score_hand(hand):
    total_matches = {5: 0, 4: 0, 3: 0, 2: 0, 1: 0, 0: 0}
    jokers = 0

    for char in chars:
        matches = len([1 for card in hand if card == char])
        if char == 'J':
            jokers = matches
        else:
            total_matches[matches] += 1

    for i in range(6):
        if total_matches[i] > 0:
            max_matches = i

    if max_matches == 5 or max_matches + jokers == 5:
        return '6'
    elif max_matches == 4 or max_matches + jokers == 4:
        return '5'
    elif max_matches == 3 and total_matches[2] == 1 or total_matches[2] == 2 and jokers == 1 or max_matches == 3 and jokers == 1:
        return '4'
    elif max_matches == 3 or max_matches + jokers == 3:
        return '3'
    elif total_matches[2] == 2:
        return '2'
    elif total_matches[2] == 1 or max_matches + jokers == 2:
        return '1'
    else:
        return '0'

def score_tie_hand(hand):
    new_hand = hand
    for char in tie_mapping:
       new_hand = new_hand.replace(char, tie_mapping[char])

    return new_hand

scored_hands = [int(score_hand(hand) + score_tie_hand(hand)) for hand in hands]

ranked_hands = [sorted(scored_hands).index(hand) + 1 for hand in scored_hands]

print(sum([rank * bid for rank, bid in zip(ranked_hands, bids)]))
