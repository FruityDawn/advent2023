with open('input.txt', 'r') as f:
    lines = f.read().splitlines()

expanse_rows = set(list(range(len(lines))))
expanse_cols = set(list(range(len(lines[0]))))

galaxies = []

for r in range(len(lines)):
    for c in range(len(lines[0])):
        if lines[r][c] == '#':
            if r in expanse_rows:
                expanse_rows.remove(r)

            if c in expanse_cols:
                expanse_cols.remove(c)
            galaxies.append((r, c))

sum = 0

for x in range(len(galaxies) - 1):
    for y in range(x, len(galaxies)):
        r1, c1 = galaxies[x]
        r2, c2 = galaxies[y]

        prev_dist = abs(r1 - r2) + abs(c1 - c2)

        prev_dist += (1000000 - 1) * len([r for r in expanse_rows if r > min(r1, r2) and r < max(r1, r2)])
        prev_dist += (1000000 - 1) * len([c for c in expanse_cols if c > min(c1, c2) and c < max(c1, c2)])

        sum += prev_dist

print(sum)
