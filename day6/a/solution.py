with open('input.txt', 'r') as f:
    times = []
    distances = []
    for line in f.read().splitlines():
        type_, nums = line.split(':')

        if type_ == 'Time':
            for num in nums.split(' '):
                if len(num) > 0:
                    times.append(int(num))
        elif type_ == 'Distance':
            for num in nums.split(' '):
                if len(num) > 0:
                    distances.append(int(num))

total = 1

for i in range(len(times)):
    total *= len([x for x in range(1, times[i] + 1) if x * (times[i] - x) > distances[i]])

print(total)
