with open('input.txt', 'r') as f:
    for line in f.read().splitlines():
        type_, nums = line.split(':')

        if type_ == 'Time':
            time = int(nums.replace(' ', ''))
        elif type_ == 'Distance':
            distance = int(nums.replace(' ', ''))

 
print(len([x for x in range(1, time + 1) if x * (time - x) > distance]))

det = (time**2 - 4 * distance)**0.5

low = (time - det) / 2
high = (time + det) / 2

print(int(high - low))
