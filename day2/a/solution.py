bag = {'red': 12, 'green': 13, 'blue': 14}

game_sum = 0
with open('input.txt', 'r') as f:
    for line in f.readlines():
        game, rounds = line.split(':')[0], line.split(':')[1]
        game_num = int(game.split(' ')[-1])

        possible = True
        for round in rounds.split(';'):
            for cube in round.split(','):
                num = int(cube.strip().split(' ')[0])
                colour = cube.strip().split(' ')[1]

                if not (colour in bag and bag[colour] >= num):
                    possible = False
                    break
            if possible is False:
                break

        if possible:
            game_sum += game_num

print(game_sum)

