game_sum = 0
with open('input.txt', 'r') as f:
    for line in f.readlines():
        game, rounds = line.split(':')[0], line.split(':')[1]
        game_num = int(game.split(' ')[-1])

        min_bag = {'red': 0, 'green': 0, 'blue': 0}

        for round in rounds.split(';'):
            for cube in round.split(','):
                num = int(cube.strip().split(' ')[0])
                colour = cube.strip().split(' ')[1]

                if min_bag[colour] < num:
                    min_bag[colour] = num

        game_sum += min_bag['red'] * min_bag['green'] * min_bag['blue']

print(game_sum)

